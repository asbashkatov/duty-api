<?php
namespace Deployer;

require 'recipe/symfony.php';

// Project name
set('application', 'duty-api');

// Project repository
set('repository', 'git@gitlab.com:asbashkatov/duty-api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['public/uploads/tasks', 'config/jwt']);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('prod')
    ->setHostname('duty-api.adv2ls.ru')
    ->set('branch', 'main')
    ->setRemoteUser('deployer')
    ->set('deploy_path', '/var/www/{{application}}');
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');

