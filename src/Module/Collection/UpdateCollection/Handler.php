<?php

declare(strict_types=1);

namespace App\Module\Collection\UpdateCollection;

use App\Entity\Collection;

class Handler
{
    public function handle(Collection $collection, Command $command): Collection
    {
        return $collection
            ->setName(mb_strtolower($command->getName()))
            ->setDescription($command->getDescription());
    }
}
