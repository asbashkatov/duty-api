<?php

declare(strict_types=1);

namespace App\Module\Collection\RemoveCollection;

use App\Entity\Collection;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(Collection $collection): void
    {
        if ($collection->getTasks()->count() > 0) {
            throw new \DomainException('Unable to delete collection: collection is not empty.');
        }
        $this->em->remove($collection);
    }

}
