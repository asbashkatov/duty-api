<?php

declare(strict_types=1);

namespace App\Module\Collection\AddNewCollection;

use App\Entity\Collection;

class Handler
{
    public function handle(Command $command): Collection
    {
        return (new Collection())
            ->setName(mb_strtolower($command->getName()))
            ->setDescription($command->getDescription());
    }
}
