<?php

declare(strict_types=1);

namespace App\Module\Auth\SignUpByEmail;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    private const PASSWORD_REGEX = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/';
    private const PASSWORD_VALIDATION_ERROR = '"Пароль должен быть не менее 6 символов в длину, содержать цифры, спец символы и символы латинского алфавита в верхнем и нижнем регистрах."';

    #[Assert\Email]
    private string $email;

    #[Assert\NotBlank]
    #[Assert\Regex(self::PASSWORD_REGEX, message: self::PASSWORD_VALIDATION_ERROR)]
    private string $password;

    private string $username;

    public function __construct(string $email = '', string $password = '', string $username = '')
    {
        $this->email    = \mb_strtolower(trim($email));
        $this->password = $password;
        $this->username = $username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }
}
