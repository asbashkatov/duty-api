<?php

declare(strict_types=1);

namespace App\Module\Task\UpdateAudioFile;

use App\Entity\Task;
use App\Service\TaskFileService;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Handler
{
    private TaskFileService $fileService;

    public function __construct(TaskFileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function handle(Task $task, UploadedFile $file): Task
    {
        if ($file->getClientOriginalExtension() !== Task::EXT_MP3) {
            throw new \InvalidArgumentException('The uploaded file must have the mp3 extension.');
        }
        $file->move(
            $this->fileService->getFullFileDirPath($task),
            $this->fileService->getFileName($task)
        );
        return $task;
    }
}
