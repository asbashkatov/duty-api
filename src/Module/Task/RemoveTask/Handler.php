<?php

declare(strict_types=1);

namespace App\Module\Task\RemoveTask;

use App\Entity\Task;
use App\Service\TaskFileService;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{
    private TaskFileService $taskFileService;

    private EntityManagerInterface $em;

    public function __construct(TaskFileService $taskFileService, EntityManagerInterface $em)
    {
        $this->taskFileService = $taskFileService;
        $this->em = $em;
    }

    public function handle(Task $task): void
    {
        $filePath = $this->taskFileService->getFullFilePath($task);
        if (\file_exists($filePath)) {
            \unlink($filePath);
        }
        $this->em->remove($task);
    }
}
