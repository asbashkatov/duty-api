<?php

declare(strict_types=1);

namespace App\Module\Task\AddNewTask;

use App\Entity\Collection;
use App\Entity\Task;

class Handler
{
    public function handle(Command $command, Collection $collection): Task
    {
        return (new Task())
            ->setCollection($collection)
            ->setQuestion($command->getQuestion())
            ->setAnswer(mb_strtolower($command->getAnswer()))
            ->setFileExt($command->getFileExt())
            ->setCost($command->getCost());
    }
}
