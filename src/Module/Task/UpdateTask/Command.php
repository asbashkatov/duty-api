<?php

declare(strict_types=1);

namespace App\Module\Task\UpdateTask;

class Command
{
    private string $fileExt;
    private string $question;
    private string $answer;
    private int $cost;

    public function __construct(string $fileExt, string $question, string $answer, int $cost)
    {
        $this->fileExt  = $fileExt;
        $this->question = $question;
        $this->answer   = $answer;
        $this->cost     = $cost;
    }

    public function getFileExt(): string
    {
        return $this->fileExt;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function getCost(): int
    {
        return $this->cost;
    }
}
