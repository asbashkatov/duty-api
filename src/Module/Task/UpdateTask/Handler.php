<?php

declare(strict_types=1);

namespace App\Module\Task\UpdateTask;

use App\Entity\Task;

class Handler
{
    public function handle(Task $task, Command $command): Task
    {
        return $task
            ->setFileExt($command->getFileExt())
            ->setCost($command->getCost())
            ->setAnswer(mb_strtolower($command->getAnswer()))
            ->setQuestion($command->getQuestion());
    }
}
