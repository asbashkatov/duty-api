<?php

namespace App\Client;

use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AudioUploadClient
{
    public function __construct(
        private string $apiHost,
        private string $apiVersion,
        private string $accessToken,
        private HttpClientInterface $httpClient
    )
    {
    }

    public function getAudioUploadLink(): string
    {
        $response = $this->httpClient->request(
            'POST',
            "{$this->apiHost}/method/marusia.getAudioUploadLink?access_token={$this->accessToken}&v={$this->apiVersion}"
        );
        $content = $response->toArray();

        return (string)$content['response']['audio_upload_link'];
    }

    public function uploadFile(string $filePath, string $audioUploadLink): array
    {
        $formFields = [
            'file' => DataPart::fromPath($filePath),
        ];
        $formData = new FormDataPart($formFields);
        $response = $this->httpClient->request(
            'POST',
            "{$audioUploadLink}&access_token={$this->accessToken}&v={$this->apiVersion}",
            [
                'headers' => $formData->getPreparedHeaders()->toArray(),
                'body' => $formData->bodyToIterable(),
            ]
        );

        return $response->toArray();
    }

    public function createAudio(array $audio_meta): int
    {
        $audio_meta_string = json_encode($audio_meta);
        $response = $this->httpClient->request(
            'POST',
            "{$this->apiHost}/method/marusia.createAudio?access_token={$this->accessToken}&v={$this->apiVersion}&audio_meta={$audio_meta_string}"
        );
        $content = $response->toArray();

        return (int)$content['response']['id'];
    }
}