<?php

namespace App\Service;

use App\Client\AudioUploadClient;
use App\Entity\Task;
use App\Repository\TaskRepository;

class TaskUploadService
{
    public function __construct(
        private AudioUploadClient $uploadClient,
        private TaskRepository $taskRepository,
        private TaskFileService $taskFileService
    )
    {
    }

    public function uploadTask(Task $task): bool
    {
        $uploadLink = $this->uploadClient->getAudioUploadLink();

        $filePath = $this->taskFileService->getFullFilePath($task);
        $audioMeta = $this->uploadClient->uploadFile($filePath, $uploadLink);

        $vkAudioId = $this->uploadClient->createAudio($audioMeta);

        $task->setVkAudioId($vkAudioId);
        $this->taskRepository->add($task, true);

        return true;
    }
}