<?php

namespace App\Service;

use function morphos\Russian\pluralize;

class MorphService
{
    public function pluralize(int $value, string $object, ?string $case = null): string
    {
        return pluralize($value, $object, false, $case);
    }
}