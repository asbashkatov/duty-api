<?php

namespace App\Service;

use App\Entity\Collection;
use App\Enum\NextAction;
use App\MarusiaResponse\BaseResponse;
use App\MarusiaResponse\PlayerResponse;
use App\MarusiaResponse\SimpleResponse;
use App\Repository\CollectionRepository;
use App\Repository\TaskRepository;
use App\ValueObject\PlayerData;
use morphos\Gender;
use morphos\Russian\CardinalNumeralGenerator;

class MarusiaResponseService
{
    public function __construct(
        private CollectionRepository $collectionRepository,
        private TaskRepository $taskRepository,
        private TaskFileService $taskFileService,
        private MorphService $morphService,
    )
    {
    }

    public function getResponse(array $requestData): BaseResponse
    {
        $action = $requestData['state']['session']['action'] ?? null;
        $action = NextAction::from($action);

        $userMessage = mb_strtolower($requestData['request']['command'] ?? '');
        if ($userMessage === mb_strtolower('Выбрать коллекцию')) {
            $action = NextAction::COLLECTION_SHOW_LIST;
        }

        switch ($action) {
            case NextAction::COLLECTION_SELECTED:
                return $this->collectionSelected($userMessage, $requestData);
                break;
            case NextAction::TASK_LISTENED:
                $taskId = $requestData['state']['session']['task_id'] ?? 0;
                return $this->taskListened($taskId, $requestData);
                break;
            case NextAction::TASK_ANSWERED:
                $taskId = $requestData['state']['session']['task_id'] ?? 0;
                $collectionScore = $requestData['state']['session']['collectionScore'] ?? 0;
                return $this->taskAnswered($userMessage, $taskId, $collectionScore, $requestData);
                break;
            case NextAction::COLLECTION_SHOW_LIST:
                return $this->collectionShowList($requestData);
                break;
            default:
                $isFirstMessage = ($requestData['session']['message_id'] ?? 0) === 0;

                return $this->defaultMessage($isFirstMessage, $requestData);
        }
    }

    private function defaultMessage(bool $isFirstMessage, array $requestData): BaseResponse
    {
        $collections = $this->collectionRepository->findAll();
        $buttons = array_map(fn(Collection $c) => $c->getName(), $collections);
        array_unshift($buttons, 'Выбрать коллекцию');

        $message = $isFirstMessage ?
            "Привет!👋 <break time=\"200ms\"/>Я навык для изучения английского языка. \n\nВыберите коллекцию заданий,<break time=\"100ms\"/> прослушайте запись диалога на английском и ответьте на вопросы. Так вы научитесь легче воспринимать английскую речь на слух.<break time=\"200ms\"/> \n\nКакую коллекцию вопросов хотите пройти? <break time=\"100ms\"/>Произнесите название коллекции или скажите \"Выбрать коллекцию\"."
            : 'Какую коллекцию вопросов хотите пройти? <break time=\"100ms\"/>Произнесите название коллекции или скажите "Выбрать коллекцию".';
        return new SimpleResponse(
            strip_tags($message),
            $message,
            NextAction::COLLECTION_SELECTED,
            [],
            $buttons,
            $requestData
        );
    }

    private function collectionShowList(array $requestData): BaseResponse
    {
        $collections = $this->collectionRepository->findAll();
        $buttons = array_map(fn(Collection $c) => $c->getName(), $collections);

        $message = "Список доступных коллекций: \n\n";
        foreach ($buttons as $button) {
            $message .= " - {$button}<break time=\"500ms\"/>\n";
        }
        $message .= PHP_EOL . '<break time="1s"/>Произнесите название коллекции для начала. Например, скажите "диалоги о лондоне".';

        return new SimpleResponse(
            strip_tags($message),
            $message,
            NextAction::COLLECTION_SELECTED,
            [],
            $buttons,
            $requestData
        );
    }

    private function taskListened(int $taskId, array $requestData): BaseResponse
    {
        $task = $taskId > 0 ? $this->taskRepository->findOneById($taskId): null;

        if ($task === null) {
            $collections = $this->collectionRepository->findAll();
            $buttons = array_map(fn(Collection $c) => $c->getName(), $collections);

            return new SimpleResponse(
                "Задание не найдено, давайте попробуем еще раз выбрать коллекцию? Произнесите название коллекции или скажите \"Выбрать коллекцию\".",
                null,
                NextAction::COLLECTION_SELECTED,
                [],
                $buttons,
                $requestData
            );
        }

        return new SimpleResponse(
            "Внимание, вопрос №{$task->getId()}.\n {$task->getQuestion()}",
            null,
            NextAction::TASK_ANSWERED,
            ['task_id' => $task->getId()],
            [],
            $requestData
        );
    }

    private function taskAnswered(string $answer, int $taskId, int $collectionScore, array $requestData): BaseResponse
    {
        $task = $taskId > 0 ? $this->taskRepository->findOneById($taskId): null;

        if ($task === null) {
            $collections = $this->collectionRepository->findAll();
            $buttons = array_map(fn(Collection $c) => $c->getName(), $collections);

            return new SimpleResponse(
                "Задание не найдено, давайте попробуем еще раз выбрать коллекцию? Произнесите название коллекции или скажите \"Выбрать коллекцию\".",
                null,
                NextAction::COLLECTION_SELECTED,
                [],
                $buttons,
                $requestData
            );
        }
        
        $isCorrectAnswer = mb_strtolower($answer) === mb_strtolower($task->getAnswer());
        if ($isCorrectAnswer) {
            $collectionScore+= $task->getCost();
        }
        $nextTask = $this->taskRepository->getNextTaskByCollectionAndLastId($task->getCollection(), $taskId);
        if (null === $nextTask) {
            // всю коллекцию прошел
            $collectionScoreStr = $this->morphService->pluralize($collectionScore, 'балл');

            $message = $isCorrectAnswer ?
                'Правильно! <speaker audio=marusia-sounds/game-win-1>':
                "<speaker audio=marusia-sounds/game-loss-3>Неправильно!\n Правильный ответ был - {$task->getAnswer()}.";
            $message .= "\n\nПоздравляю, вы изучили всю коллекцию заданий и набрали {$collectionScoreStr}. \n\nДавайте выберем следующую коллекцию? Произнесите \"Выбрать коллекцию\".";

            return new SimpleResponse(
                strip_tags($message),
                $message,
                NextAction::EMPTY,
                ['collectionScore' => $collectionScore],
                ['Выбрать коллекцию'],
                $requestData
            );
        } else {
            // еще есть задания
            $message = $isCorrectAnswer ?
                'Правильно! <speaker audio=marusia-sounds/game-win-1>':
                "<speaker audio=marusia-sounds/game-loss-3>Неправильно!\n Правильный ответ был - {$task->getAnswer()}.";
            $message .= "\n\nПрослушайте следующее задание и скажите \"перейти к вопросу\".";
            return new PlayerResponse(
                strip_tags($message),
                $message,
                NextAction::TASK_LISTENED,
                ['task_id' => $nextTask->getId(), 'collectionScore' => $collectionScore],
                ['перейти к вопросу'],
                new PlayerData(
                    'task_' . $nextTask->getId(),
                    $this->taskFileService->getFullFileUrl($nextTask),
                    "Задание №{$nextTask->getId()} из коллекции \"{$nextTask->getCollection()->getName()}\""
                ),
                $requestData
            );
        }
    }

    private function collectionSelected(string $collectionName, array $requestData): BaseResponse
    {
        $collection = $this->collectionRepository->findOneByName($collectionName);

        if ($collection === null) {
            return $this->defaultMessage(false, $requestData);
        }

        $task = $this->taskRepository->getNextTaskByCollectionAndLastId($collection);

        if ($task == null) {
            $message = "Поздравляю! Вы прошли всю коллекцию \"{$collection->getName()}\". \nДавайте выберем следующую коллекцию? Произнесите \"Выбрать коллекцию\".";
            $messageTts = "Поздравляю! Вы прошли всю коллекцию \"{$collection->getName()}\". Давайте выберем следующую коллекцию? Произнесите \"Выбрать коллекцию\".";

            $collections = $this->collectionRepository->findAll();
            $buttons = array_map(fn(Collection $c) => $c->getName(), $collections);

            return new SimpleResponse(
                $message,
                $messageTts,
                NextAction::COLLECTION_SELECTED,
                [],
                $buttons,
                $requestData
            );
        }

        $taskCount = $collection->getTasks()->count();
        $pluralizeTaskCount = $this->morphService->pluralize($taskCount, 'задание', 'genitive');
        $getCaseTaskCount = CardinalNumeralGenerator::getCase($taskCount, 'genitive', Gender::MALE);
        $getCaseTaskCount = str_replace((string)$taskCount, $getCaseTaskCount, $pluralizeTaskCount);
        $message = "Выбрана коллекция \"{$collection->getName()}\" из {$pluralizeTaskCount}.\n Прослушайте задание и скажите \"перейти к вопросу\".";
        $messageTts = "Выбрана коллекция \"{$collection->getName()}\" из {$getCaseTaskCount}.\n Прослушайте задание и скажите \"перейти к вопросу\".";
        return new PlayerResponse(
            $message,
            $messageTts,
            NextAction::TASK_LISTENED,
            ['task_id' => $task->getId(), 'collectionScore' => 0],
            ['перейти к вопросу'],
            new PlayerData(
                'task_' . $task->getId(),
                $this->taskFileService->getFullFileUrl($task),
                "Задание №{$task->getId()} из коллекции \"{$collection->getName()}\"",
            ),
            $requestData
        );
    }
}