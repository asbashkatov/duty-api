<?php

namespace App\Service;

use App\Entity\Task;
use App\ValueObject\AudioFileData;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\UrlHelper;

class TaskFileService
{
    public function __construct(
        private string $taskDirectory,
        private UrlHelper $urlHelper
    )
    {
    }

    #[Pure] public function getFileName(Task $task): string
    {
        return $task->getId() . '.' . $task->getFileExt();
    }

    public function getFullFileDirPath(Task $task): string
    {
        $collectionId = $task->getCollection()->getId();
        return sprintf(
            '%s/%d/%d/',
            $this->taskDirectory,
            $collectionId % 1000,
            $collectionId
        );
    }

    public function getFullFilePath(Task $task): string
    {
        $collectionId = $task->getCollection()->getId();
        return sprintf(
            '%s/%d/%d/%s.%s',
            $this->taskDirectory,
            $collectionId % 1000,
            $collectionId,
            $task->getId(),
            $task->getFileExt()
        );
    }

    public function getFullFileUrl(Task $task): string
    {
        $collectionId = $task->getCollection()->getId();
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/tasks/%d/%d/%s.%s',
                $collectionId % 1000,
                $collectionId,
                $task->getId(),
                $task->getFileExt()
            )
        );
    }

    public function getFileData(Task $task): AudioFileData
    {
        $fullFilePath = $this->getFullFilePath($task);
        return new AudioFileData(
            $this->getFullFileUrl($task),
            \file_exists($fullFilePath)
        );
    }
}
