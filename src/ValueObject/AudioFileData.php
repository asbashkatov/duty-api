<?php

declare(strict_types=1);

namespace App\ValueObject;

class AudioFileData
{
    public readonly string $fullFileUrl;
    public readonly bool $isFileExists;

    public function __construct(string $fullFileUrl, bool $isFileExists)
    {
        $this->fullFileUrl  = $fullFileUrl;
        $this->isFileExists = $isFileExists;
    }
}
