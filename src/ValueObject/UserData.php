<?php

declare(strict_types=1);

namespace App\ValueObject;

use App\Entity\User;

class UserData
{
    public readonly string $email;
    public readonly string $emailHash;
    public readonly string $username;
    public readonly array $roles;

    public function __construct(User $user)
    {
        $this->email    = $user->getEmail();
        $this->username = $user->getUsername();
        $this->roles    = $user->getRoles();

        $this->emailHash = md5(\mb_strtolower(trim($user->getEmail())));
    }
}
