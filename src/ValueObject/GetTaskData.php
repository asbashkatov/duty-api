<?php

declare(strict_types=1);

namespace App\ValueObject;

use App\Entity\Task;

class GetTaskData
{
    public readonly int $id;
    public readonly string $fileExt;
    public readonly string $question;
    public readonly string $answer;
    public readonly int $cost;
    public readonly ?int $vkAudioId;
    public readonly ?int $collectionId;
    public readonly string $fullFileUrl;
    public readonly bool $isFileExists;

    public function __construct(Task $task, AudioFileData $audioFileData)
    {
        $this->id = $task->getId();
        $this->fileExt = $task->getFileExt();
        $this->question = $task->getQuestion();
        $this->answer = $task->getAnswer();
        $this->cost = (int)$task->getCost();
        $this->vkAudioId = $task->getVkAudioId();
        $this->collectionId = $task->getCollection()?->getId();
        $this->fullFileUrl = $audioFileData->fullFileUrl;
        $this->isFileExists = $audioFileData->isFileExists;
    }
}
