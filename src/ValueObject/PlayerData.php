<?php

namespace App\ValueObject;

class PlayerData
{
    public readonly string $trackId;
    public readonly string $fileUrl;
    public readonly string $subTitle;
    public readonly string $title;
    public readonly string $imageUrl;

    public function __construct(string $trackId, string $fileUrl, string $title)
    {
        $this->trackId = $trackId;
        $this->fileUrl = $fileUrl;
        $this->title = $title;
        $this->subTitle = 'Команда Dudes';
        $this->imageUrl = 'https://img.championat.com/i/k/h/16340288771124074508.jpg'; //TODO: заменить на превью
    }
}