<?php

declare(strict_types=1);

namespace App\ValueObject;

class PaginatorData
{
    public readonly int $offset;
    public readonly int $limit;

    public function __construct(int $page, int $limit)
    {
        if ($page < 1 || $limit < 1) {
            throw new \InvalidArgumentException('Page and limit must be gte 1');
        }
        $this->offset = ($page - 1) * $limit;
        $this->limit  = $limit;
    }
}
