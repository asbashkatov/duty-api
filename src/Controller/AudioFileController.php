<?php

namespace App\Controller;

use App\Entity\Task;
use App\Module\Task\UpdateAudioFile\Handler as UpdateFileHandler;
use App\Service\TaskFileService;
use App\ValueObject\GetTaskData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AudioFileController extends AbstractController
{
    private Serializer $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/tasks/{task<\d+>}/audio', name: 'api_tasks_audio_post', methods: ['POST'])]
    public function index(Task $task, Request $request, UpdateFileHandler $handler, TaskFileService $taskFileService): Response
    {
        if ($task->getId() <= 11) {
            throw new AccessDeniedException();
        }
        $file = $request->files->get('audio');
        if (empty($file)) {
            throw new \InvalidArgumentException('A required file is missing');
        }
        $task = $handler->handle($task, $file);
        $taskData = new GetTaskData($task, $taskFileService->getFileData($task));
        $data = $this->serializer->normalize($taskData);
        return $this->json($data);
    }
}
