<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SystemController extends AbstractController
{
    #[Route('/api/health-check', name: 'api_health_check', methods: 'GET')]
    public function healthCheck(): Response
    {
        return new JsonResponse(status: Response::HTTP_NO_CONTENT);
    }
    #[Route('/api/secure-check', name: 'api_secure_check', methods: 'GET')]
    public function secureCheck(): Response
    {
        return new JsonResponse(status: Response::HTTP_NO_CONTENT);
    }
}
