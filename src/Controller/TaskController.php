<?php

namespace App\Controller;

use App\Entity\Collection;
use App\Entity\Task;
use App\Module\Task\AddNewTask\Command as AddTaskCommand;
use App\Module\Task\AddNewTask\Handler as AddTaskHandler;
use App\Module\Task\RemoveTask\Handler as RemoveTaskHandler;
use App\Module\Task\UpdateTask\Command as UpdateTaskCommand;
use App\Module\Task\UpdateTask\Handler as UpdateTaskHandler;
use App\Repository\TaskRepository;
use App\Service\TaskFileService;
use App\ValueObject\GetTaskData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class TaskController extends AbstractController
{
    private Serializer $serializer;

    private EntityManagerInterface $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections/{collection<\d+>}/tasks', name: 'api_collections_tasks_create', methods: ['POST'])]
    public function createTask(Collection $collection, Request $request, AddTaskHandler $handler): Response
    {
        $command = $this->serializer->deserialize($request->getContent(), AddTaskCommand::class, JsonEncoder::FORMAT);
        $task = $handler->handle($command, $collection);
        $this->em->persist($task);
        $this->em->flush();

        $data = $this->serializer->normalize($task);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/tasks/{task<\d+>}', name: 'api_tasks_patch', methods: ['PATCH'])]
    public function updateTask(Task $task, Request $request, UpdateTaskHandler $handler): Response
    {
        if ($task->getId() <= 11) {
            throw new AccessDeniedException();
        }
        $command = $this->serializer->deserialize($request->getContent(), UpdateTaskCommand::class, JsonEncoder::FORMAT);
        $task = $handler->handle($task, $command);
        $this->em->flush();

        $data = $this->serializer->normalize($task);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/tasks/{task<\d+>}', name: 'api_tasks_get', methods: ['GET'])]
    public function getTask(Task $task, TaskFileService $taskFileService): Response
    {
        $taskData = new GetTaskData($task, $taskFileService->getFileData($task));
        $data = $this->serializer->normalize($taskData);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections/{collection<\d+>}/tasks', name: 'api_collections_tasks_get_list', methods: ['GET'])]
    public function getTaskList(Collection $collection, TaskRepository $taskRepository, TaskFileService $taskFileService): Response
    {
        $tasks = $taskRepository->findBy([
            'collection' => $collection,
        ]);
        $tasksData = \array_map(
            static fn(Task $task) => new GetTaskData($task, $taskFileService->getFileData($task)),
            $tasks
        );
        $data = $this->serializer->normalize($tasksData);
        $response = $this->json($data);
        $response->headers->set('X-Total-Count', \count($tasks));
        return $response;
    }

    #[Route('/api/tasks/{task<\d+>}', name: 'api_tasks_delete', methods: ['DELETE'])]
    public function removeTask(Task $task, RemoveTaskHandler $handler): Response
    {
        if ($task->getId() <= 11) {
            throw new AccessDeniedException();
        }
        $handler->handle($task);
        $this->em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
