<?php

namespace App\Controller;

use App\Entity\User;
use App\Module\Auth\SignUpByEmail\Command;
use App\Module\Auth\SignUpByEmail\Handler as SignUpHandler;
use App\ValueObject\UserData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    private Serializer $serializer;

    private EntityManagerInterface $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/registration', name: 'api_registration', methods: 'POST')]
    public function registration(SignUpHandler $handler, Request $request): Response
    {
        $command = $this->serializer->deserialize($request->getContent(), Command::class, JsonEncoder::FORMAT);
        $user = $handler->handle($command);
        $this->em->persist($user);
        $this->em->flush();
        $userData = new UserData($user);
        $data = $this->serializer->normalize($userData);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/whoAmI', name: 'api_whoAmI', methods: ['GET'])]
    public function whoAmI(): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->json(null, Response::HTTP_NO_CONTENT);
        }
        $userData = new UserData($user);
        $data = $this->serializer->normalize($userData);
        return $this->json($data);
    }

    #[Route('/api/login', name: 'api_login', methods: 'POST')]
    public function login(): Response
    {
        return $this->json([]);
    }
}
