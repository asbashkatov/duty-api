<?php

namespace App\Controller;

use App\Service\MarusiaResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarusiaHandlerController extends AbstractController
{
    public function __construct(
        private MarusiaResponseService $marusiaResponseService
    )
    {
    }

    #[Route('/api/marusia/handler', name: 'api_marusia_handler', methods: 'POST')]
    public function handle(Request $request): Response
    {
        $requestData = $request->toArray();
        $response = $this->marusiaResponseService->getResponse($requestData);
        return $this->json($response->toArray());

//        $request_message = $request->toArray();
//        return $this->json([
//            'response' => [
//                'buttons' => [['title' => 'Перейти к вопросам']],
//                'text' => "text text",
//                'end_session' => false,
//                'audio_player' => [
//                    'seek_track' => 0,
//                    'seek_second' => 0,
//                    'playlist' => [
//                        [
//                            'stream' => [
//                                'track_id' => 'task_1',
//                                'source_type' => 'url',
//                                'source' => 'https://0970-5-101-21-245.ngrok.io/uploads/tasks/2/2/1.mp3'
//                            ],
//                            'meta' => [
//                                'sub_title' => 'Исполнитель',
//                                'title' => 'Название композиции',
//                                'art' => [
//                                    'url' => 'https://img.championat.com/i/k/h/16340288771124074508.jpg'
//                                ]
//                            ]
//                        ]
//                    ]
//                ]
//            ],
//            'session' => [
//                'user_id' => $request_message['session']['user_id'],
//                'session_id' => $request_message['session']['session_id'],
//                'message_id' => $request_message['session']['message_id']
//            ],
//            'version' => $request_message['version']
//        ]);
    }
}
