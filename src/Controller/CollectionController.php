<?php

namespace App\Controller;

use App\Entity\Collection;
use App\Module\Collection\AddNewCollection\Command;
use App\Module\Collection\AddNewCollection\Handler;
use App\Module\Collection\RemoveCollection\Handler as RemoveCollectionHandler;
use App\Module\Collection\UpdateCollection\Command as UpdateCollectionCommand;
use App\Module\Collection\UpdateCollection\Handler as UpdateCollectionHandler;
use App\Repository\CollectionRepository;
use App\ValueObject\PaginatorData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class CollectionController extends AbstractController
{
    private Serializer $serializer;

    private EntityManagerInterface $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections', name: 'api_collections_list_get', methods: ['GET'])]
    public function collectionsList(Request $request, CollectionRepository $repository): Response
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 20);
        $paginator = new PaginatorData($page, $limit);
        $collections = $repository->findBy([], null, $paginator->limit, $paginator->offset);
        $data = $this->serializer->normalize($collections);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections/{collection<\d+>}', name: 'api_collections_get', methods: ['GET'])]
    public function getCollection(Collection $collection): Response
    {
        $data = $this->serializer->normalize($collection);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections', name: 'api_collections_post', methods: ['POST'])]
    public function createCollection(Request $request, Handler $handler): Response
    {
        $command = $this->serializer->deserialize($request->getContent(), Command::class, JsonEncoder::FORMAT);
        $task = $handler->handle($command);
        $this->em->persist($task);
        $this->em->flush();

        $data = $this->serializer->normalize($task);
        return $this->json($data);
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/collections/{collection<\d+>}', name: 'api_collections_patch', methods: ['PATCH'])]
    public function updateCollection(Collection $collection, Request $request, UpdateCollectionHandler $handler): Response
    {
        if ($collection->getId() <= 9) {
            throw new AccessDeniedException();
        }
        $command = $this->serializer->deserialize($request->getContent(), UpdateCollectionCommand::class, JsonEncoder::FORMAT);
        $collection = $handler->handle($collection, $command);
        $this->em->flush();

        $data = $this->serializer->normalize($collection);
        return $this->json($data);
    }

    #[Route('/api/collections/{collection<\d+>}', name: 'api_collections_delete', methods: ['DELETE'])]
    public function removeCollection(Collection $collection, RemoveCollectionHandler $handler): Response
    {
        if ($collection->getId() <= 9) {
            throw new AccessDeniedException();
        }
        $handler->handle($collection);
        $this->em->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
