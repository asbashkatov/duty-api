<?php

namespace App\MarusiaResponse;

use App\ValueObject\PlayerData;

abstract class BaseResponse
{
    protected string $text = '';
    protected string $tts = '';
    protected bool $endSession = false;
    protected array $request;
    protected array $sessionState = [];
    protected array $userStateUpdate = [];
    protected array $buttons = [];
    protected ?PlayerData $playerData = null;

    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function toArray(): array
    {
        $data = [
            'response' => [
                'text'          => $this->text,
                'tts'           => !empty($this->tts) ? $this->tts : $this->text,
                'end_session'   => $this->endSession,
                'buttons'       => array_map(fn(string $btnText) => ['title' => $btnText], $this->buttons)
            ],
            'session' => [
                'user_id'       => $this->request['session']['user_id'],
                'session_id'    => $this->request['session']['session_id'],
                'message_id'    => $this->request['session']['message_id']
            ],
            'session_state' => array_merge($this->request['state']['session'] ?? [], $this->sessionState),
            'version' => $this->request['version']
        ];

        if (!empty($this->userStateUpdate)) {
            $data['user_state_update'] = $this->userStateUpdate;
        }

        if (!empty($this->tts) && str_contains($this->tts, '<break')) {
            unset($data['response']['tts']);
            $data['response']['tts_type'] = 'ssml';
            $ssml =<<<SSML
<?xml version="1.0" encoding="UTF-8"?>
<speak version="1.1" xmlns:mailru="[http://vc.go.mail.ru]" lang="ru">{$this->tts}</speak>
SSML;
            $data['response']['ssml'] = $ssml;
        }

        if (null !== $this->playerData) {
            $data['response']['audio_player'] = [
                'seek_track' => 0,
                'seek_second' => 0,
                'playlist' => [
                    [
                        'stream' => [
                            'track_id' => $this->playerData->trackId,
                            'source_type' => 'url',
                            'source' => $this->playerData->fileUrl
                        ],
                        'meta' => [
                            'sub_title' => $this->playerData->subTitle,
                            'title' => $this->playerData->title,
                            'art' => [
                                'url' => $this->playerData->imageUrl
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $data;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    public function getTts(): string
    {
        return $this->tts;
    }

    public function setTts(string $tts): self
    {
        $this->tts = $tts;
        return $this;
    }

    public function isEndSession(): bool
    {
        return $this->endSession;
    }

    public function setEndSession(bool $endSession): self
    {
        $this->endSession = $endSession;
        return $this;
    }

    public function getSessionState(): array
    {
        return $this->sessionState;
    }

    public function setSessionState(array $sessionState): self
    {
        $this->sessionState = $sessionState;

        return $this;
    }

    public function getButtons(): array
    {
        return $this->buttons;
    }

    public function setButtons(array $buttons): self
    {
        $this->buttons = $buttons;

        return $this;
    }


}