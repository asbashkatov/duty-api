<?php

namespace App\MarusiaResponse;

use App\Enum\NextAction;
use App\ValueObject\PlayerData;

class PlayerResponse extends BaseResponse
{

    public function __construct(
        string $text,
        string $tts,
        NextAction $nextAction,
        array $payload,
        array $buttons,
        PlayerData $playerData,
        array $request
    )
    {
        $this->request = $request;
        $this->text = $text;
        $this->tts = $tts;
        $this->sessionState = array_merge($payload, ['action' => $nextAction->value]);
        $this->buttons = $buttons;
        $this->playerData = $playerData;
    }
}