<?php

namespace App\MarusiaResponse;

use App\Enum\NextAction;

class SimpleResponse extends BaseResponse
{
    public function __construct(string $text, ?string $tts, NextAction $nextAction, array $payload, array $buttons, array $request)
    {
        $this->request = $request;
        $this->text = $text;
        $this->tts = $tts ?? $text;
        $this->sessionState = array_merge($payload, ['action' => $nextAction->value]);
        $this->buttons = $buttons;
    }
}