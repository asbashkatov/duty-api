<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    public const EXT_MP3 = 'mp3';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Ignore]
    #[ORM\ManyToOne(targetEntity: Collection::class, inversedBy: 'tasks')]
    #[ORM\JoinColumn(nullable: false)]
    private Collection $collection;

    #[ORM\Column(type: 'string', length: 50)]
    private string $file_ext;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $vk_audio_id = null;

    #[ORM\Column(type: 'text')]
    private string $question;

    #[ORM\Column(type: 'string', length: 500)]
    private string $answer;

    #[ORM\Column(type: 'integer')]
    private string $cost;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCollection(): ?Collection
    {
        return $this->collection;
    }

    public function setCollection(?Collection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getFileExt(): ?string
    {
        return $this->file_ext;
    }

    public function setFileExt(string $file_ext): self
    {
        $this->file_ext = $file_ext;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getVkAudioId(): ?int
    {
        return $this->vk_audio_id;
    }

    public function setVkAudioId(?int $vk_audio_id): self
    {
        $this->vk_audio_id = $vk_audio_id;

        return $this;
    }
}
