<?php

namespace App\Command;

use App\Entity\Collection;
use App\Entity\Task;
use App\Repository\CollectionRepository;
use App\Repository\TaskRepository;
use App\Service\TaskUploadService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-test-data',
    description: 'Create test data',
)]
class CreateTestDataCommand extends Command
{
    public function __construct(
        private CollectionRepository $collectionRepository,
        private TaskRepository $taskRepository,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $collection = new Collection();
        $collection
            ->setName('📒 Фразовые глаголы')
            ->setDescription('В этой коллекции собраны звуковые карточки для изучения фразовых глаголов.');

        $this->collectionRepository->add($collection, true);

        $task1 = new Task();
        $task1
            ->setQuestion('Что не говорили в этом диалоге?')
            ->setAnswer('про работу')
            ->setCollection($collection)
            ->setCost(1)
            ->setFileExt(Task::EXT_MP3);

        $this->taskRepository->add($task1, true);

        $io->success('Test data added');

        return Command::SUCCESS;
    }
}
