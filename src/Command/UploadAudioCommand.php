<?php

namespace App\Command;

use App\Repository\TaskRepository;
use App\Service\TaskUploadService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:upload-audio',
    description: 'Upload audio',
)]
class UploadAudioCommand extends Command
{
    public function __construct(
        private TaskRepository $taskRepository,
        private TaskUploadService $taskUploadService,
        string $name = null
    )
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $tasks = $this->taskRepository->findNeedUploadTasks();
        foreach ($tasks as $task) {
            $uploaded = $this->taskUploadService->uploadTask($task);
            if ($uploaded) {
                $io->success("Task {$task->getId()} success uploaded.");
            } else {
                $io->error("Task {$task->getId()} not uploaded.");
            }
        }

        $io->success('Done.');

        return Command::SUCCESS;
    }
}
