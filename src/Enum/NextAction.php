<?php

namespace App\Enum;

enum NextAction: string
{
    case EMPTY = '';
    case COLLECTION_SELECTED = 'collection_selected';
    case COLLECTION_SHOW_LIST = 'collection_show_list';
    case TASK_LISTENED = 'task_listened';
    case TASK_ANSWERED = 'task_answered';

}